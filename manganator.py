#!/usr/bin/python3

import os
import sys
import argparse
import zipfile
from natsort import natsorted
import string
from random import choice, randint

def main():
    args = args_parser()

    path = get_path(args.directory)

    if (args.rename_dirs):
        rename_directories(path)

    if (args.rename_files):
        rename_files(path)

    if (args.rename_sub_files):
        rename_sub_files(path)

    if (args.zip):
        zip_files(path)

def safe_rename(src, dst):
    if (os.path.exists(dst)):
        if (os.path.isdir(dst)):
            raise IsADirectoryError("{}: Already exists".format(dst))
        if (os.path.isfile(dst)):
            raise FileExistsError("{}: Already exists".format(dst))
        raise Exception("Unknown exception")
    os.rename(src, dst)

def rename_directories(path):
    while True:
        root, dirnames, filenames = next(os.walk(path))
        srcs = list()
        dsts = list()
        rnds = list()
        counter = 0
        for dirname in natsorted(dirnames):
            counter += 1
            src = dirname
            zfill = len(str(len(dirnames)))
            if (zfill < 3):
                zfill = 3
            dst = str(counter).zfill(zfill) + ".0"
            if (src != dst):
                srcs.append(src)
                dsts.append(dst)
                rnds.append(rand_string())
        if (len(srcs) == 0):
            return
        for src, dst in zip(srcs, dsts):
            print("{} -> {}".format(src, dst))
        print("root: {}".format(root))
        if (yes_or_no()):
            for src, rnd in zip(srcs, rnds):
                target_src = os.path.join(root, src)
                target_dst = os.path.join(root, rnd)
                safe_rename(target_src, target_dst)
            for rnd, dst in zip(rnds, dsts):
                target_src = os.path.join(root, rnd)
                target_dst = os.path.join(root, dst)
                safe_rename(target_src, target_dst)
            return

def rename_files(path):
    while True:
        root, dirnames, filenames = next(os.walk(path))
        srcs = list()
        dsts = list()
        rnds = list()
        counter = 0
        for filename in natsorted(filenames):
            counter += 1
            name, ext = os.path.splitext(filename)
            src = name + ext
            zfill = len(str(len(filenames)))
            if (zfill < 3):
                zfill = 3
            dst = str(counter).zfill(zfill) + ext
            if (src != dst):
                srcs.append(src)
                dsts.append(dst)
                rnds.append(rand_string())
        if (len(srcs) == 0):
            return
        print("root: {}".format(root))
        for src, dst in zip(srcs, dsts):
            print("{} -> {}".format(src, dst))
        if (yes_or_no()):
            for src, rnd in zip(srcs, rnds):
                target_src = os.path.join(root, src)
                target_dst = os.path.join(root, rnd)
                safe_rename(target_src, target_dst)
            for rnd, dst in zip(rnds, dsts):
                target_src = os.path.join(root, rnd)
                target_dst = os.path.join(root, dst)
                safe_rename(target_src, target_dst)
            return

def rename_sub_files(directory):
    root, dirnames, filenames = next(os.walk(directory))
    for dirname in natsorted(dirnames):
        rename_files(os.path.join(root, dirname))

def zip_files(directory):
    if not (os.path.isdir(directory)):
        return

    zf_dst = "{}.cbz".format(os.path.basename(os.path.dirname(directory)))
    print("zf_dst: {}".format(zf_dst))

    zf = zipfile.ZipFile(zf_dst, mode='x', compression=zipfile.ZIP_DEFLATED)
    for root, dirs, files in os.walk(directory):
        for name in files:
            zf.write(os.path.join(root, name),
                arcname=os.path.join(root, name).replace(directory, ''))
    zf.close()

    zf = zipfile.ZipFile(zf_dst, mode='r', compression=zipfile.ZIP_DEFLATED)
    if (zf.testzip() == None):
        print("ZipFile valid")
    zf.close()

def args_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("directory", help="Directory to process",
        type=str)
    parser.add_argument("--rename-dirs",
        help="Rename the directories within the directory",
        action="store_true")
    parser.add_argument("--rename-sub-files",
        help="Rename files in directories within the directory",
        action="store_true")
    parser.add_argument("--rename-files", help="Rename the files",
        action="store_true")
    parser.add_argument("--zip", help="Zip the contents of directory",
        action="store_true")
    args = parser.parse_args()
    return(args)

def get_path(directory):
    if not os.path.isabs(directory):
        directory = os.path.abspath(directory)
    if not os.path.isdir(directory):
        raise(FileNotFoundError)
    if not directory.endswith(os.sep):
        directory += os.sep
    return(directory)

def pause():
    try:
        input("Press enter to continue")
    except (KeyboardInterrupt):
        print()
        sys.exit(1)
    finally:
        print()

def rand_string():
    min_char = 12
    max_char = 12
    allchar = string.ascii_letters + string.digits
    return("".join(choice(allchar) for x in range(
        randint(min_char, max_char))))

def yes_or_no():
    answer = True
    try:
        i = input("Is this okay ([Y]es/[N]o/[Q]uit): ")
        i = i.lower()
        if (i == 'n'):
            answer = False
        if (i == 'q'):
            sys.exit(0)
    except (KeyboardInterrupt):
        print()
        sys.exit(1)
    finally:
        print()
    return(answer)

if __name__ == '__main__':
    main()
